const { claimWinByDisconnection } = require('./gameInteractions');

const input = {
  gameAddress: process.argv[2]
};

if (!input.gameAddress) throw new Error('Define the game address as the first param');
const execute = async () => {
  console.log('Claiming win by disconnection');
  return claimWinByDisconnection(input.gameAddress);
};

execute()
  .then(() => console.log('Completed'))
  .catch(err => {
    console.log('Error', err);
  });
