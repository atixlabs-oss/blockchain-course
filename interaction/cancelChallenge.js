const { getAccounts } = require('./networkHelper');
const { cancelChallenge } = require('./gameInteractions');

const input = {
  accountIndex: process.argv[2] || 0
};

const execute = async () => {
  const accounts = await getAccounts();
  const challengeCreator = accounts[input.accountIndex];
  console.log(`Cancelling the challenge from ${challengeCreator}`);
  return cancelChallenge(challengeCreator);
};

execute()
  .then(() => console.log('Completed'))
  .catch(err => {
    console.log('Error', err);
  });
