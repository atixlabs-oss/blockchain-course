const { getAccounts } = require('./networkHelper');
const { createGame } = require('./gameInteractions');

const input = {
  playerOneAccountIndex: process.argv[2] || 0,
  playerTwoAccountIndex: process.argv[3] || 1,
  betInWeis: process.argv[4] || '1000',
  maxBlocksPerMove: process.argv[5] || 20
};

const execute = async () => {
  const accounts = await getAccounts();
  const playerOne = accounts[input.playerOneAccountIndex];
  const playerTwo = accounts[input.playerTwoAccountIndex];
  console.log(
    `Creating a game with ${playerOne} as the challenge creator(player one) and ${playerTwo} as the accepter(player two). The bet is ${input.betInWeis} weis and the max blocks per move is ${input.maxBlocksPerMove}`
  );
  return createGame(playerOne, playerTwo, input.betInWeis, input.maxBlocksPerMove);
};

execute()
  .then(() => console.log('Completed'))
  .catch(err => {
    console.log('Error', err);
  });
