const { printBoard } = require('./gameInteractions');

const input = {
  gameAddress: process.argv[2]
};

if (!input.gameAddress) throw new Error('Define the game address as the first param');

const execute = () => printBoard(input.gameAddress);

execute()
  .then(() => console.log('Completed'))
  .catch(err => {
    console.log('Error', err);
  });
