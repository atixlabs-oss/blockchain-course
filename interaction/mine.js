const { advanceBlocks } = require('./networkHelper');

const input = {
  numberOfBlocks: parseInt(process.argv[2] || 1, 10)
};

const execute = async () => {
  console.log(`Mininig ${input.numberOfBlocks}`);
  return advanceBlocks(input.numberOfBlocks);
};

execute()
  .then(() => console.log('Completed'))
  .catch(err => {
    console.log('Error', err);
  });
