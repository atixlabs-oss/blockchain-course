const Web3 = require('web3');
const HDWalletProvider = require('truffle-hdwallet-provider');
const abiDecoder = require('abi-decoder');
const _ = require('lodash');
const truffleConfig = require('../truffle-config');

const getHostProvider = network =>
  `http://${truffleConfig.networks[network].host}:${truffleConfig.networks[network].port}`;
const getDefaultProvider = network =>
  truffleConfig.networks[network].provider || getHostProvider(network);
const getNetworkHost = network => truffleConfig.networks[network].host;

const getWeb3 = (network = 'development', options) => {
  const provider = network
    ? getDefaultProvider(network)
    : new HDWalletProvider(options.pk, options.host);
  return new Web3(provider);
};

const currentTimestamp = async web3 => {
  const lastBlock = await web3.eth.getBlock('latest');
  return lastBlock.timestamp;
};

const decodeLog = async (web3, contractJson, eventName, tx) => {
  abiDecoder.addABI(contractJson.abi);
  const txReceipt = await new web3.eth.getTransactionReceipt(tx);
  console.log('Decoding Tx receipt', _.pick(txReceipt, ['transactionHash', 'from', 'to']));

  const decoded = abiDecoder.decodeLogs(txReceipt.logs);
  const event = decoded.find(it => it && it.name === eventName);

  console.log(`${eventName} event:`, event.events);
  return event;
};

// Theory behind
// https://ethereum.stackexchange.com/questions/11030/how-can-i-deploy-a-contract-with-reference-to-a-library-contract-without-using-a
// Important: libraryAddress must be without the initial a 0x
const linkLibrary = (contract, libraryName, libraryAddress) => {
  // eslint-disable-next-line no-param-reassign
  contract.bytecode = contract.bytecode.replace(new RegExp(`_*${libraryName}_*`), libraryAddress);
};

const getAccounts = () => {
  const web3 = getWeb3();
  return web3.eth.getAccounts();
};

const advanceBlock = () =>
  new Promise((resolve, reject) => {
    const web3 = getWeb3();
    web3.currentProvider.send(
      {
        jsonrpc: '2.0',
        method: 'evm_mine',
        id: new Date().getTime()
      },
      err => {
        if (err) {
          return reject(err);
        }
        return resolve();
      }
    );
  });
const advanceBlocks = async numberOfBlocks =>
  Promise.all([...Array(numberOfBlocks)].map(advanceBlock));

const getBalance = async address => {
  const web3 = await getWeb3();
  return web3.eth.getBalance(address);
};
module.exports = {
  getWeb3,
  getNetworkHost,
  currentTimestamp,
  decodeLog,
  linkLibrary,
  getAccounts,
  advanceBlocks,
  getBalance
};
