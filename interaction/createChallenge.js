const { getAccounts } = require('./networkHelper');
const { createChallenge } = require('./gameInteractions');

const input = {
  accountIndex: process.argv[2] || 0,
  betInWeis: process.argv[3] || '1000',
  maxBlocksPerMove: process.argv[4] || 20
};

const execute = async () => {
  const accounts = await getAccounts();
  const challengeCreator = accounts[input.accountIndex];
  console.log(
    `Creating a challenge with ${challengeCreator} as the challenge creator. The bet is ${input.betInWeis} weis and the max blocks per move is ${input.maxBlocksPerMove}`
  );
  return createChallenge(challengeCreator, input.betInWeis, input.maxBlocksPerMove);
};

execute()
  .then(() => console.log('Completed'))
  .catch(err => {
    console.log('Error', err);
  });
