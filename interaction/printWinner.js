const { getWinner } = require('./gameInteractions');

const input = {
  gameAddress: process.argv[2]
};

if (!input.gameAddress) throw new Error('Define the game address as the first param');
const execute = async () => {
  const possibleWinner = await getWinner(input.gameAddress);
  console.log(`The winner address is ${possibleWinner}!`);
};

execute()
  .then(() => console.log('Completed'))
  .catch(err => {
    console.log('Error', err);
  });
