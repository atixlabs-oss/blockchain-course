const config = require('./script-config.json');
const { getWeb3, getAccounts } = require('./networkHelper');
const Factory = require('../build/contracts/TicTacToeFactory.json');
const Game = require('../build/contracts/TicTacToe.json');

const createChallenge = async (challengeCreator, betInWeis, maxBlocksPerMove) => {
  const web3 = getWeb3();
  const factory = await new web3.eth.Contract(Factory.abi, config.factory);

  // TODO Complete the following line, it is missing something
  // Hint: Remember to transfer the bet!
  return factory.methods.createChallenge(maxBlocksPerMove);
};

const cancelChallenge = async challengeCreator => {
  const web3 = getWeb3();
  const factory = await new web3.eth.Contract(Factory.abi, config.factory);

  return factory.methods.cancelChallenge().send({ from: challengeCreator });
};

const acceptChallenge = async (challengeCreator, challengeAccepter, betInWeis) => {
  const web3 = getWeb3();
  const factory = await new web3.eth.Contract(Factory.abi, config.factory);

  const result = await factory.methods
    .acceptChallenge(challengeCreator)
    .send({ from: challengeAccepter, value: betInWeis, gas: 1e7 });
  console.log(`game in ${JSON.stringify(result.events.ChallengeAccepted.returnValues.game)}`);
};

const createGame = async (playerOne, playerTwo, betInWeis, maxBlocksPerMove) => {
  await createChallenge(playerOne, betInWeis, maxBlocksPerMove);
  await acceptChallenge(playerOne, playerTwo, betInWeis);
};
const move = async (row, column, player, gameAddress) => {
  const web3 = getWeb3();
  const game = await new web3.eth.Contract(Game.abi, gameAddress);
  return game.methods.makeMove(row, column).send({ from: player });
};

const getMarkAt = async (row, column, game) => {
  const map = {
    0: '-',
    1: '1',
    2: '2'
  };
  const mark = await game.methods.board(row, column).call();
  return map[mark];
};

const printBoard = async gameAddress => {
  const web3 = getWeb3();
  const game = await new web3.eth.Contract(Game.abi, gameAddress);
  const string = `
    ${await getMarkAt(0, 0, game)} ${await getMarkAt(0, 1, game)} ${await getMarkAt(0, 2, game)}
    ${await getMarkAt(1, 0, game)} ${await getMarkAt(1, 1, game)} ${await getMarkAt(1, 2, game)}
    ${await getMarkAt(2, 0, game)} ${await getMarkAt(2, 1, game)} ${await getMarkAt(2, 2, game)}
   `;
  console.log(string);
};

const getWinner = async gameAddress => {
  const web3 = getWeb3();
  const game = await new web3.eth.Contract(Game.abi, gameAddress);
  // TODO Complete the following line, it is missing something
  return game.methods.winner();
};

const claimDraw = async gameAddress => {
  const web3 = getWeb3();
  const game = await new web3.eth.Contract(Game.abi, gameAddress);
  const accounts = await getAccounts();
  // TODO Complete the following line, it is missing something
  // Hint: from can be any account here
  return game.methods.claimDraw();
};

const claimWinByDisconnection = async gameAddress => {
  // TODO Fill all this function
};

const claimPrize = async (gameAddress, playerAddress) => {
  const web3 = getWeb3();

  const game = await new web3.eth.Contract(Game.abi, gameAddress);

  return game.methods.claimPrize().send({ from: playerAddress });
};

module.exports = {
  createChallenge,
  cancelChallenge,
  acceptChallenge,
  createGame,
  move,
  claimWinByDisconnection,
  printBoard,
  getWinner,
  claimDraw,
  claimPrize
};
