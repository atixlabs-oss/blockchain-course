const { getAccounts } = require('./networkHelper');
const { acceptChallenge } = require('./gameInteractions');

const input = {
  challengeCreatorAccountIndex: process.argv[2] || 0,
  challengeAccepterAccountIndex: process.argv[3] || 1,
  betInWeis: process.argv[4] || '1000'
};
const execute = async () => {
  const accounts = await getAccounts();
  const challengeCreator = accounts[input.challengeCreatorAccountIndex];
  const challengeAccepter = accounts[input.challengeAccepterAccountIndex];
  console.log(
    `Accepting the challenge from ${challengeCreator} with ${challengeAccepter}. Bet: ${input.betInWeis}`
  );
  return acceptChallenge(challengeCreator, challengeAccepter, input.betInWeis);
};

execute()
  .then(() => console.log('Completed'))
  .catch(err => {
    console.log('Error', err);
  });
