const { getAccounts } = require('./networkHelper');
const { claimPrize } = require('./gameInteractions');

const input = {
  gameAddress: process.argv[2],
  playerAccountIndex: process.argv[3] || 0
};

if (!input.gameAddress) throw new Error('Define the game address as the first param');
const execute = async () => {
  const accounts = await getAccounts();
  console.log(`Claiming prize of ${accounts[input.playerAccountIndex]}`);
  return claimPrize(input.gameAddress, accounts[input.playerAccountIndex]);
};

execute()
  .then(() => console.log('Completed'))
  .catch(err => {
    console.log('Error', err);
  });
