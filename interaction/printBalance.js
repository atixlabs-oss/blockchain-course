const { getAccounts, getBalance } = require('./networkHelper');

const input = {
  accountIndex: parseInt(process.argv[2] || 0, 10)
};

const execute = async () => {
  const accounts = await getAccounts();
  console.log(`Balance of ${accounts[input.accountIndex]}`);
  console.log(await getBalance(accounts[input.accountIndex]));
};

execute()
  .then(() => console.log('Completed'))
  .catch(err => {
    console.log('Error', err);
  });
