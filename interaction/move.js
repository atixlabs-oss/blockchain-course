const { getAccounts } = require('./networkHelper');
const { move, printBoard, getWinner } = require('./gameInteractions');

const input = {
  gameAddress: process.argv[2],
  playerAccountIndex: process.argv[3] || 0,
  row: process.argv[4] || 0,
  column: process.argv[5] || 0
};

if (!input.gameAddress) throw new Error('Define the game address as the first param');
const execute = async () => {
  const accounts = await getAccounts();
  const player = accounts[input.playerAccountIndex];
  console.log(
    `Marking (${input.row}, ${input.column}) with the player ${player} in game ${input.gameAddress}`
  );
  await move(input.row, input.column, player, input.gameAddress);
  await printBoard(input.gameAddress);
  const possibleWinner = await getWinner(input.gameAddress);
  if (possibleWinner !== '0x0000000000000000000000000000000000000000')
    console.log(`You won! The winner address is ${possibleWinner}!`);
};

execute()
  .then(() => console.log('Completed'))
  .catch(err => {
    console.log('Error', err);
  });
