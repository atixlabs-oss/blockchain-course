pragma solidity 0.5.11;

import "openzeppelin-solidity/contracts/math/SafeMath.sol";
import "openzeppelin-solidity/contracts/math/Math.sol";
import "./TicTacToe.sol";

contract TicTacToeFactory {
  using SafeMath for uint256;

  struct Challenge {
    uint256 bet;
    uint256 maxBlocksPerMove;
  }

  mapping (address => Challenge) public challenges;
  uint256 public minMaxBlocksPerMove;
  uint256 public maxMaxBlocksPerMove;

  event ChallengeCreated(address indexed challengeCreator, uint256 bet);
  // TODO define the missing event, the params should be called game, challengeCreator, and challengeAccepter
  event ChallengeCancelled(address indexed challengeCreator);
  constructor(uint256 _minMaxBlocksPerMove, uint256 _maxMaxBlocksPerMove) public {
    require(_minMaxBlocksPerMove <= _maxMaxBlocksPerMove, "Min max blocks per move cannot be fewer than max max blocks per move");
    minMaxBlocksPerMove = _minMaxBlocksPerMove;
    maxMaxBlocksPerMove = _maxMaxBlocksPerMove;
  }

  /**
    @notice Function called by default when sending funds, creates a challenge with the amount send
   */
  function () external payable {
    createChallenge(Math.average(minMaxBlocksPerMove, maxMaxBlocksPerMove));
  }

  /**
    @notice Creates a challenge, must have a bet sent, the sender doesnt have to have an open challenge already,
    @param maxBlocksPerMove Max amount of blocks to wait for the player before assuming the player to be disconnected
    Must be between minMaxBlocksPerMove and maxMaxBlocksPerMove
   */
  function createChallenge(uint256 maxBlocksPerMove) public payable {
    require(challenges[msg.sender].bet == 0, "There already is an open challenge");
    require(msg.value > 0, "You have to make a bet");
    require(minMaxBlocksPerMove <= maxBlocksPerMove, "Max blocks per move are too few");
    require(maxBlocksPerMove <= maxMaxBlocksPerMove, "Max blocks per move are too many");
    challenges[msg.sender] = Challenge(msg.value, maxBlocksPerMove);

    emit ChallengeCreated(msg.sender, msg.value);
  }


  /**
    @notice Cancels a challenge, the sender has to have an open challenge already, returns the funds,
    and send back some funds
    @dev It cleans internal structure too
   */
  function cancelChallenge() public {
    // TODO Complete this function
  }

  /**
    @notice Accepts a challenge, must pay the bet of the accepted challenge
    @param challengeCreator Address of the user that created the challenge, cannot be the msg sender
   */
  function acceptChallenge(address challengeCreator) public payable {
    require(challenges[challengeCreator].bet > 0, "Challenge not found");
    // TODO Check if the accepter has sent the right bet, if not revert
    // with "Not the right bet"
    require(challengeCreator != msg.sender, "You cannot take your own challenge");
    TicTacToe game = new TicTacToe(challengeCreator, msg.sender, challenges[challengeCreator].maxBlocksPerMove);
    address payable gameAddress = address(game);
    gameAddress.transfer(msg.value.mul(uint256(2)));
    delete challenges[challengeCreator];

    emit ChallengeAccepted(game, challengeCreator, msg.sender);
  }
}