pragma solidity 0.5.11;

import "openzeppelin-solidity/contracts/math/SafeMath.sol";

contract TicTacToe {
  using SafeMath for uint256;

  enum Mark {NO_MARK, PLAYER_ONE_MARK, PLAYER_TWO_MARK}
  enum Status {PLAYING, PLAYER_ONE_WON, PLAYER_TWO_WON, DRAW}

  address public playerOne;
  address public playerTwo;

  uint256 public maxBlockUntilNextMove;
  uint256 public maxBlocksPerMove;

  Mark[3][3] public board;
  Status private status;

  bool public playerOnesTurn;
  mapping (address => uint256) public claimablePrize;



  event MoveMade(address indexed player, uint256 row, uint256 column);
  event GameWon(address indexed winner);
  event Draw();
  event PrizeSent(address receiver, uint256 amount);

  constructor(address _playerOne, address _playerTwo, uint256 _maxBlocksPerMove) public {
    playerOne = _playerOne;
    playerTwo = _playerTwo;
    maxBlocksPerMove = _maxBlocksPerMove;
    maxBlockUntilNextMove = block.number.add(_maxBlocksPerMove);
    playerOnesTurn = true;
    status = Status.PLAYING; // Not really necessary
  }

  /**
    @notice Function called by default when sending funds, it does do anything more than receiving those funds
   */
  // TODO Make the game able to receive extra payments
  function () external {
  }

  /**
    @notice Limits the execution of the function to the time where the game has not been decided yet
   */
  modifier whenNotFinished() {
    require(!hasFinished(), "Game has already finished");
    _;

  }

  /**
    @notice Limits the execution of the function to the time where the game has already finished
   */
  modifier whenFinished() {
    // TODO throw revert if the game has not been finished
    // with the message "Game has not finished yet"
    // Call the function after that
  }

  /**
    @notice Makes a function callable only by the players of the game
   */
  modifier onlyPlayer() {
    require (playerOne == msg.sender || playerTwo == msg.sender, "Not a player");
    _;
  }

  /**
    @notice Makes a function callable only by the player its the turn of
   */
  modifier isPlayersTurn() {
    require (
      playerOnesTurn && playerOne == msg.sender ||
      !playerOnesTurn && playerTwo == msg.sender,
      "Not the player turn"
    );
    _;
  }

  /**
    @notice Marks a cell with the senders mark. It must be the players turn
    and the cell must be unnocupied. It checks if the game has been won automatically
    and if so it emits a GameWon event
    @param row Row of the cell
    @param column Column of the cell
   */
  function makeMove(uint256 row, uint256 column) public whenNotFinished onlyPlayer isPlayersTurn {
    require (block.number <= maxBlockUntilNextMove, "Too late to make a move");
    require(board[row][column] == Mark.NO_MARK, "Already occupied space");
    board[row][column] = msg.sender == playerOne ? Mark.PLAYER_ONE_MARK : Mark.PLAYER_TWO_MARK;
    emit MoveMade(msg.sender, row, column);
    if (isWinningPosition(row, column)) {
      markWinner(msg.sender);
    } else {
      maxBlockUntilNextMove = block.number.add(maxBlocksPerMove);
    }
    playerOnesTurn = !playerOnesTurn;
  }

  /**
    @notice Gives the victory to the player who has not disconnected.
    It checks if the max amount of blocks per move has passed and the player of this turn
    is declared loser.
    It assigns the prize to the winner.
   */
  function claimWinByDisconnection() {
    // TODO Check if the necessary conditions are met and set the correct winner
    // You should add the visibility, the payability(if necessary) and the pureness
    // Hint: You can use one of the extra modifers declared above
  }


  /**
    @notice If there is a drawing position, it assigns the prizes, emits an event
    and declares the game a draw
    @dev It checks if no more moves can be done. It doesnt check if the board
    has a winning position but that is given by
    the automaticity of the makeMove when someone wins as that function finishes
    the game as soon as someone wins
   */
  function claimDraw() public whenNotFinished {
    uint64 i;
    uint64 j;
    for (i = 0; i < 3; i++ ) {
      for (j = 0; j < 3; j++ ) {
        require(board[i][j] != Mark.NO_MARK, "Cell unmarked");
      }
    }
    assignPrize(playerOne, address(this).balance.div(2));
    assignPrize(playerTwo, address(this).balance.div(2));
    status = Status.DRAW;
    emit Draw();
  }
  /**
    @notice Checks if the game has a winning position. A hinting cell must be provided
    @param hintRow Row that the hinting cell belongs to
    @param hintColumn Column that the hinting cell belongs to
   */
  function isWinningPosition(uint256 hintRow, uint256 hintColumn) public view returns (bool) {
    uint64 i;
    // Colum
    Mark expectedMark = board[hintRow][hintColumn];
    bool wonByColumn = true;
    for (i = 0; i < 3; i++) {
      wonByColumn = wonByColumn && board[i][hintColumn] == expectedMark;
    }
    // Row
    bool wonByRow = true;
    for (i = 0; i < 3; i++) {
      wonByRow = wonByRow && board[hintRow][i] == expectedMark;
    }
    // 1st Diagonal
    bool wonBy1stDiagonal = true;
    for (i = 0; i < 3; i++) {
      wonBy1stDiagonal = wonBy1stDiagonal && board[i][i] == expectedMark;
    }
    // 2nd Diagonal
    bool wonBy2ndDiagonal = true;
    for (i = 0; i < 3; i++) {
      wonBy2ndDiagonal = wonBy2ndDiagonal && board[i][uint256(2).sub(i)] == expectedMark;
    }

    return wonByColumn || wonByRow || wonBy1stDiagonal || wonBy2ndDiagonal;
  }

  /**
    @notice Claims the assigned price for the sender
    If there is no assigned prize then there will be a transaction of 0
   */
  function claimPrize() public {
    // TODO Send the price and emit the necessary events
  }

  /**
    @notice Checks if game has finished. For the game to be finished
    the game has to be claimed a draw, a win or win through makeMove
   */
  function hasFinished() public view returns(bool) {
    return status != Status.PLAYING;
  }

  /**
    @notice Checks if the game has been declared a draw
   */
  function gameIsADraw() public view returns(bool) {
    return status == Status.DRAW;
  }

  /**
    @notice Checks if the game has been won
   */
  function hasBeenWon() public view returns(bool) {
    return hasFinished() && !gameIsADraw();
  }

  /**
    @notice Returns the address of the winner. If the game has not been
    won then a null address is returned
   */
  function winner() public view returns(address){
    if (!hasBeenWon())
      return address(0);
    return status == Status.PLAYER_ONE_WON ? playerOne : playerTwo;
  }

  /**
    @notice Set the necessary variables to make some price
    claimable by the user
   */
  // TODO add visibility to function
  function assignPrize(address player, uint256 amount) {
    claimablePrize[player] = amount;
  }

  /**
    @notice Marks a player as the winner and declares the game won
   */
  function markWinner(address player) private {
    status = player == playerOne ? Status.PLAYER_ONE_WON : Status.PLAYER_TWO_WON;
    assignPrize(player, address(this).balance);
    emit GameWon(player);
  }
}
