const { expectEvent, expectRevert, BN } = require('openzeppelin-test-helpers');
const { expect, use } = require('chai');

const dirtyChai = require('dirty-chai');
const bnChai = require('bn-chai');

const {
  getFactory,
  DEFAULT_MAX_BLOCKS_PER_MOVE,
  ONE_ETHER,
  getBalance,
  getGasCost
} = require('./helpers/testHelper');

use(dirtyChai);
use(bnChai(BN));

describe('Factory tests-Cancell Challenge', function() {
  let factory;
  before(async function() {
    factory = await getFactory();
  });

  contract('GIVEN there is a factory of TicTacToes with an open challenge', function([anUser]) {
    const challengeCreator = anUser;
    let result;
    let previousBalance;
    let gasCost;
    before(async function() {
      await factory.createChallenge(DEFAULT_MAX_BLOCKS_PER_MOVE, {
        from: challengeCreator,
        value: ONE_ETHER
      });
    });
    describe('WHEN the challenge is cancelled', function() {
      before(async function() {
        previousBalance = await getBalance(challengeCreator);
        result = await factory.cancelChallenge({ from: challengeCreator });
        gasCost = await getGasCost(result);
      });

      it('THEN an event is emitted', async function() {
        expectEvent.inLogs(result.logs, 'ChallengeCancelled', {
          challengeCreator
        });
      });

      it('THEN the balance of the factory decreases', async function() {
        expect(await getBalance(factory.address)).to.be.eq.BN(new BN(0));
      });
      it('THEN the balance of the challenge creator increases', async function() {
        expect(await getBalance(challengeCreator)).to.be.eq.BN(
          previousBalance.add(ONE_ETHER).sub(gasCost)
        );
      });
    });
  });

  contract('GIVEN there is a factory of TicTacToes with a cancelled challenge ', function([
    anUser,
    anotherUser
  ]) {
    const challengeCreator = anUser;
    before(async function() {
      await factory.createChallenge(DEFAULT_MAX_BLOCKS_PER_MOVE, {
        from: challengeCreator,
        value: ONE_ETHER
      });
      await factory.cancelChallenge({ from: challengeCreator });
    });
    describe('WHEN the challenge is tried to be accepted', function() {
      it('THEN the tx fails', async function() {
        await expectRevert(
          factory.acceptChallenge(challengeCreator, { from: anotherUser }),
          'Challenge not found'
        );
      });
    });
  });

  contract('GIVEN there is a factory of TicTacToes with a cancelled challenge ', function([
    anUser
  ]) {
    const challengeCreator = anUser;
    before(async function() {
      await factory.createChallenge(DEFAULT_MAX_BLOCKS_PER_MOVE, {
        from: challengeCreator,
        value: ONE_ETHER
      });
      await factory.cancelChallenge({ from: challengeCreator });
    });
    describe('WHEN the challenge is tried to be cancelled again', function() {
      it('THEN the tx fails', async function() {
        await expectRevert(
          factory.cancelChallenge({ from: challengeCreator }),
          'Challenge not found'
        );
      });
    });
  });

  contract('GIVEN there is a factory of TicTacToes with a cancelled challenge ', function([
    anUser
  ]) {
    const challengeCreator = anUser;
    before(async function() {
      await factory.createChallenge(DEFAULT_MAX_BLOCKS_PER_MOVE, {
        from: challengeCreator,
        value: ONE_ETHER
      });
      await factory.cancelChallenge({ from: challengeCreator });
    });
    describe('WHEN a new challenge is tried to be created from the same account', function() {
      let result;
      let previousBalance;
      let gasCost;
      before(async function() {
        previousBalance = await getBalance(challengeCreator);
        result = await factory.createChallenge(DEFAULT_MAX_BLOCKS_PER_MOVE, {
          from: challengeCreator,
          value: ONE_ETHER
        });
        gasCost = await getGasCost(result);
      });

      it('THEN an event is emitted', async function() {
        expectEvent.inLogs(result.logs, 'ChallengeCreated', {
          challengeCreator,
          bet: ONE_ETHER
        });
      });

      it('THEN the balance of the factory increases', async function() {
        expect(await getBalance(factory.address)).to.eq.BN(ONE_ETHER);
      });
      it('THEN the balance of the challenge creator decreases', async function() {
        expect(await getBalance(challengeCreator)).to.eq.BN(
          previousBalance.sub(ONE_ETHER).sub(gasCost)
        );
      });
    });
  });

  contract(
    'GIVEN there is a factory of TicTacToes with a user who has an open challenge',
    function([anUser, anotherUser]) {
      const challengeCreator = anUser;

      before(async function() {
        await factory.createChallenge(DEFAULT_MAX_BLOCKS_PER_MOVE, {
          from: challengeCreator,
          value: ONE_ETHER
        });
      });
      describe('WHEN another user tries to cancel a challenge ', function() {
        it('THEN the tx fails', async function() {
          await expectRevert(factory.cancelChallenge({ from: anotherUser }), 'Challenge not found');
        });
      });
    }
  );
});
