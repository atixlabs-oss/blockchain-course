const { expectRevert, expectEvent, BN } = require('openzeppelin-test-helpers');
const { expect, use } = require('chai');

const dirtyChai = require('dirty-chai');
const bnChai = require('bn-chai');
const { createGame } = require('./helpers/testHelper');

use(dirtyChai);
use(bnChai(BN));

describe('Game tests-Disconnection', function() {
  contract('GIVEN there is a fresh game', function([playerOne, playerTwo]) {
    let game;
    before(async function() {
      game = await createGame({ playerOne, playerTwo });
      // No marks
    });
    describe('WHEN the player two tries to move', function() {
      it('THEN the tx fails', async function() {
        await expectRevert(game.makeMove(0, 0, { from: playerTwo }), 'Not the player turn');
      });
    });
  });

  contract('GIVEN there is a fresh game', function([playerOne, playerTwo]) {
    let game;
    before(async function() {
      game = await createGame({ playerOne, playerTwo });
    });
    describe('WHEN the player one tries to move', function() {
      let result;
      before(async function() {
        result = await game.makeMove(0, 0, { from: playerOne });
      });
      it('THEN an event is emmitted', async function() {
        await expectEvent.inLogs(result.logs, 'MoveMade', {
          player: playerOne,
          row: new BN(0),
          column: new BN(0)
        });
      });
      it('THEN the it´s not the player one turn anymore', async function() {
        expect(await game.playerOnesTurn()).to.be.false();
      });
    });
  });

  contract('GIVEN there is an open game and its the player two turn', function([
    playerOne,
    playerTwo
  ]) {
    let game;
    before(async function() {
      game = await createGame({ playerOne, playerTwo });
      await game.makeMove(0, 0, { from: playerOne });
    });
    // eslint-disable-next-line mocha/no-identical-title
    describe('WHEN the player one tries to move', function() {
      it('THEN the tx fails', async function() {
        await expectRevert(game.makeMove(0, 1, { from: playerOne }), 'Not the player turn');
      });
    });
  });

  contract('GIVEN there is an open game and its the player two turn', function([
    playerOne,
    playerTwo
  ]) {
    let game;
    before(async function() {
      game = await createGame({ playerOne, playerTwo });
      await game.makeMove(1, 1, { from: playerOne });
    });
    // eslint-disable-next-line mocha/no-identical-title
    describe('WHEN the player two moves', function() {
      let result;
      before(async function() {
        result = await game.makeMove(0, 0, { from: playerTwo });
      });
      it('THEN an event is emmitted', async function() {
        await expectEvent.inLogs(result.logs, 'MoveMade', {
          player: playerTwo,
          row: new BN(0),
          column: new BN(0)
        });
      });
      it('THEN it´s the playerOne turn again', async function() {
        expect(await game.playerOnesTurn()).to.be.true();
      });
    });
  });
  contract('GIVEN there is an open game with some marks on it', function([playerOne, playerTwo]) {
    let game;
    before(async function() {
      game = await createGame({ playerOne, playerTwo });
      await game.makeMove(0, 0, { from: playerOne });
    });
    // eslint-disable-next-line mocha/no-identical-title
    describe('WHEN a player tries to make a mark where is alrady one', function() {
      it('THEN the tx fails', async function() {
        await expectRevert(game.makeMove(0, 0, { from: playerTwo }), 'Already occupied space');
      });
    });
  });

  contract('GIVEN there is an open game', function([playerOne, playerTwo, anotherUser]) {
    let game;
    before(async function() {
      game = await createGame({ playerOne, playerTwo });
      // No marks
    });
    // eslint-disable-next-line mocha/no-identical-title
    describe('WHEN another user tries to move', function() {
      it('THEN the tx fails', async function() {
        await expectRevert(game.makeMove(0, 0, { from: anotherUser }), 'Not a player');
      });
    });
  });
  contract('GIVEN a won game', function([playerOne, playerTwo]) {
    let game;
    before(async function() {
      game = await createGame({ playerOne, playerTwo });

      // x x x
      // o o
      //
      await game.makeMove(0, 0, { from: playerOne });
      await game.makeMove(1, 0, { from: playerTwo });
      await game.makeMove(0, 1, { from: playerOne });
      await game.makeMove(1, 1, { from: playerTwo });
      await game.makeMove(0, 2, { from: playerOne });
    });
    // eslint-disable-next-line mocha/no-identical-title
    describe('WHEN a player tries to move', function() {
      it('THEN the tx fails', async function() {
        await expectRevert(game.makeMove(1, 2, { from: playerTwo }), 'Game has already finished');
      });
    });
  });
});
