const { expectRevert, expectEvent, BN } = require('openzeppelin-test-helpers');
const { expect, use } = require('chai');

const dirtyChai = require('dirty-chai');
const bnChai = require('bn-chai');
const {
  advanceBlocks,
  DEFAULT_MAX_BLOCKS_PER_MOVE,
  createGame,
  DEFAULT_BET
} = require('./helpers/testHelper');

use(dirtyChai);
use(bnChai(BN));

// TODO Remove skip when ready to make this tests
describe.skip('Game tests-Disconnection', function() {
  contract('GIVEN there is an open game and the playerOne didnt play for enough time', function([
    playerOne,
    playerTwo
  ]) {
    let game;
    before(async function() {
      game = await createGame({ playerOne, playerTwo });
      // TODO make the blockchain to advance the right amount of blocks
    });
    describe('WHEN the playerTwo claims the victory', function() {
      let result;
      before(async function() {
        result = await game.claimWinByDisconnection({
          from: playerTwo
        });
      });

      it('THEN an event is emitted', async function() {
        await expectEvent.inLogs(result.logs, 'GameWon', {
          winner: playerTwo
        });
      });

      it('THEN the winner is the playerTwo', async function() {
        expect(await game.winner()).to.be.equal(playerTwo);
      });
      it('THEN the playerTwo is assigned all the prize(both bets)', async function() {
        expect(await game.claimablePrize(playerTwo)).to.be.eq.BN(DEFAULT_BET.mul(new BN(2)));
      });

      it('THEN the game has been won', async function() {
        expect(await game.hasBeenWon()).to.be.true();
      });

      it('THEN the game is not a draw', async function() {
        expect(await game.gameIsADraw()).to.be.false();
      });

      it('THEN the game has finished', async function() {
        expect(await game.hasFinished()).to.be.true();
      });
    });
  });

  contract('GIVEN there is an open game and the playerOne didnt play for enough time', function([
    playerOne,
    playerTwo
  ]) {
    let game;
    before(async function() {
      game = await createGame({ playerOne, playerTwo });
      await advanceBlocks(DEFAULT_MAX_BLOCKS_PER_MOVE);
    });
    describe('WHEN the player one tries to move', function() {
      it('THEN the tx fails', async function() {
        await expectRevert(
          game.makeMove(1, 1, {
            from: playerOne
          }),
          'Too late to make a move'
        );
      });
    });
  });

  contract('GIVEN there is an open game and the playerTwo didnt play for enough time', function([
    playerOne,
    playerTwo
  ]) {
    let game;
    before(async function() {
      game = await createGame({ playerOne, playerTwo });
      await game.makeMove(1, 1, { from: playerOne });
      await advanceBlocks(DEFAULT_MAX_BLOCKS_PER_MOVE);
    });
    describe('WHEN the playerOne claims the victory', function() {
      let result;
      before(async function() {
        result = await game.claimWinByDisconnection({
          from: playerOne
        });
      });

      it('THEN an event is emitted', async function() {
        await expectEvent.inLogs(result.logs, 'GameWon', {
          winner: playerOne
        });
      });

      it('THEN the winner is the playerTwo', async function() {
        expect(await game.winner()).to.be.equal(playerOne);
      });
      it('THEN the playerTwo is assigned all the prize(both bets)', async function() {
        expect(await game.claimablePrize(playerOne)).to.be.eq.BN(DEFAULT_BET.mul(new BN(2)));
      });

      it('THEN the game has been won', async function() {
        expect(await game.hasBeenWon()).to.be.true();
      });

      it('THEN the game is not a draw', async function() {
        expect(await game.gameIsADraw()).to.be.false();
      });

      it('THEN the game has finished', async function() {
        expect(await game.hasFinished()).to.be.true();
      });
    });
  });

  contract('GIVEN there is an open game and the playerOne didnt play for enough time', function([
    playerOne,
    playerTwo,
    anotherUser
  ]) {
    let game;
    before(async function() {
      game = await createGame({ playerOne, playerTwo });
      await advanceBlocks(DEFAULT_MAX_BLOCKS_PER_MOVE);
    });
    describe('WHEN another user claims the victory', function() {
      let result;
      before(async function() {
        result = await game.claimWinByDisconnection({
          from: anotherUser
        });
      });

      it('THEN an event is emitted', async function() {
        await expectEvent.inLogs(result.logs, 'GameWon', {
          winner: playerTwo
        });
      });

      it('THEN the winner is the playerTwo', async function() {
        expect(await game.winner()).to.be.equal(playerTwo);
      });
      it('THEN the playerTwo is assigned all the prize(both bets)', async function() {
        expect(await game.claimablePrize(playerTwo)).to.be.eq.BN(DEFAULT_BET.mul(new BN(2)));
      });

      it('THEN the game has been won', async function() {
        expect(await game.hasBeenWon()).to.be.true();
      });

      it('THEN the game is not a draw', async function() {
        expect(await game.gameIsADraw()).to.be.false();
      });

      it('THEN the game has finished', async function() {
        expect(await game.hasFinished()).to.be.true();
      });
    });
  });

  contract(
    'GIVEN there is an open game and the playerOne didnt play for almost the max amount of blocks',
    function([playerOne, playerTwo]) {
      let game;
      before(async function() {
        game = await createGame({ playerOne, playerTwo });
        await advanceBlocks(DEFAULT_MAX_BLOCKS_PER_MOVE - 1);
      });
      describe('WHEN he plays', function() {
        before(async function() {
          await game.makeMove(1, 1, { from: playerOne });
        });

        it('THEN the claim win by disconnection fails', async function() {
          await expectRevert(
            game.claimWinByDisconnection({ from: playerTwo }),
            'No one has disconnected'
          );
        });

        it('THEN the playerTwo is assigned no prize', async function() {
          expect(await game.claimablePrize(playerTwo)).to.be.eq.BN(new BN(0));
        });

        it('THEN the game has not been won', async function() {
          expect(await game.hasBeenWon()).to.be.false();
        });

        it('THEN the game is not a draw', async function() {
          expect(await game.gameIsADraw()).to.be.false();
        });

        it('THEN the game has finished', async function() {
          expect(await game.hasFinished()).to.be.false();
        });
      });
    }
  );

  contract(
    'GIVEN there is an open game and the playerOne waits almost the max amount of blocks, plays and the playerTwo also waits almost the max amount of blocks',
    function([playerOne, playerTwo]) {
      let game;
      before(async function() {
        game = await createGame({ playerOne, playerTwo });
        await advanceBlocks(DEFAULT_MAX_BLOCKS_PER_MOVE - 1);
        await game.makeMove(1, 1, { from: playerOne });
      });
      describe('WHEN the player one calls claim win by disconnection', function() {
        it('THEN tx fails because the threshold got updated', async function() {
          await expectRevert(
            game.claimWinByDisconnection({ from: playerTwo }),
            'No one has disconnected'
          );
        });
      });
    }
  );

  contract(
    'GIVEN there is an open game and the playerOne waits almost the max amount of blocks, plays and the playerTwo also waits almost the max amount of blocks',
    function([playerOne, playerTwo]) {
      let game;
      before(async function() {
        game = await createGame({ playerOne, playerTwo });
        await advanceBlocks(DEFAULT_MAX_BLOCKS_PER_MOVE - 1);
        await game.makeMove(1, 1, { from: playerOne });
      });
      describe('WHEN the player two makes the next move', function() {
        it('THEN tx succeeds', async function() {
          await game.makeMove(0, 1, { from: playerTwo });
        });
      });
    }
  );
});
