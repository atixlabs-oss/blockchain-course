const { expectEvent, expectRevert, BN } = require('openzeppelin-test-helpers');
const { expect, use } = require('chai');

const dirtyChai = require('dirty-chai');
const bnChai = require('bn-chai');

const {
  getFactory,
  DEFAULT_MAX_BLOCKS_PER_MOVE,
  ONE_ETHER,
  getBalance,
  getGasCost
} = require('./helpers/testHelper');

use(dirtyChai);
use(bnChai(BN));

// TODO Remove skip when ready to run this tests
describe.skip('Factory tests-Accept challenge', function() {
  let factory;
  before(async function() {
    factory = await getFactory();
  });

  contract('GIVEN there is a factory of TicTacToes with an open challenge', function([
    anUser,
    anotherUser
  ]) {
    const challengeCreator = anUser;
    const challengeAccepter = anotherUser;
    let result;
    let previousBalance;
    let gasCost;

    before(async function() {
      await factory.createChallenge(DEFAULT_MAX_BLOCKS_PER_MOVE, {
        from: challengeCreator,
        value: ONE_ETHER
      });
    });

    describe('WHEN the challenge is accepted', function() {
      before(async function() {
        previousBalance = await getBalance(challengeAccepter);
        result = await factory.acceptChallenge(challengeCreator, {
          from: challengeAccepter,
          value: ONE_ETHER
        });
        gasCost = await getGasCost(result);
      });
      it('THEN an event is emitted', async function() {
        // TODO Expect event named 'ChallengeAccepted' with the
        // properties challengeCreator and challengeAccepter
        // Game should not be tested as we do not have address of it yet
      });

      it('THEN the balance of the factory decreases', async function() {
        expect(await getBalance(factory.address)).to.eq.BN(new BN(0));
      });
      it('THEN the balance of the challenge accepter decreases', async function() {
        expect(await getBalance(challengeAccepter)).to.eq.BN(
          previousBalance.sub(ONE_ETHER).sub(gasCost)
        );
      });

      it('THEN the balance of the game is twice the bet', async function() {
        expect(await getBalance(result.receipt.logs[0].args.game)).to.eq.BN(
          ONE_ETHER.mul(new BN(2))
        );
      });
    });
  });

  contract('GIVEN there is a factory of TicTacToes with a challenge that was accepted', function([
    anUser,
    anotherUser
  ]) {
    const challengeCreator = anUser;
    const challengeAccepter = anotherUser;
    let result;
    let previousBalance;
    let gasCost;
    before(async function() {
      await factory.createChallenge(DEFAULT_MAX_BLOCKS_PER_MOVE, {
        from: challengeCreator,
        value: ONE_ETHER
      });
      await factory.acceptChallenge(challengeCreator, {
        from: challengeAccepter,
        value: ONE_ETHER
      });
    });

    describe('WHEN the same challenge creator creates a new challenge', function() {
      before(async function() {
        previousBalance = await getBalance(challengeCreator);
        result = await factory.createChallenge(DEFAULT_MAX_BLOCKS_PER_MOVE, {
          from: challengeCreator,
          value: ONE_ETHER
        });
        gasCost = await getGasCost(result);
      });

      it('THEN an event is emitted', async function() {
        expectEvent.inLogs(result.logs, 'ChallengeCreated', {
          challengeCreator,
          bet: ONE_ETHER
        });
      });

      it('THEN the balance of the factory increases', async function() {
        // TODO expect the balance to be 1 ether
      });
      it('THEN the balance of the challenge creator decreases', async function() {
        expect(await getBalance(challengeCreator)).to.eq.BN(
          previousBalance.sub(ONE_ETHER).sub(gasCost)
        );
      });
    });
  });
  contract('GIVEN there is a factory of TicTacToes with a challenge that was accepted', function([
    anUser,
    anotherUser
  ]) {
    const challengeCreator = anUser;
    const challengeAccepter = anotherUser;

    before(async function() {
      await factory.createChallenge(DEFAULT_MAX_BLOCKS_PER_MOVE, {
        from: challengeCreator,
        value: ONE_ETHER
      });
      await factory.acceptChallenge(challengeCreator, {
        from: challengeAccepter,
        value: ONE_ETHER
      });
    });

    describe('WHEN the challenge tries to be accepted again', function() {
      it('THEN the tx fails', async function() {
        await expectRevert(
          factory.acceptChallenge(challengeCreator, {
            from: challengeCreator,
            value: ONE_ETHER
          }),
          'Challenge not found'
        );
      });
    });
  });
  contract('GIVEN there is a factory of TicTacToes with an open challenge', function([anUser]) {
    const challengeCreator = anUser;
    before(async function() {
      await factory.createChallenge(DEFAULT_MAX_BLOCKS_PER_MOVE, {
        from: challengeCreator,
        value: ONE_ETHER
      });
    });

    describe('WHEN the challenge is tried to be accepted by the challenge creator', function() {
      it('THEN the tx fails', async function() {
        await expectRevert(
          factory.acceptChallenge(challengeCreator, {
            from: challengeCreator,
            value: ONE_ETHER
          }),
          'You cannot take your own challenge'
        );
      });
    });
  });

  contract('GIVEN there is a factory of TicTacToes with an open challenge', function([
    anUser,
    anotherUser
  ]) {
    const challengeCreator = anUser;
    before(async function() {
      await factory.createChallenge(DEFAULT_MAX_BLOCKS_PER_MOVE, {
        from: challengeCreator,
        value: ONE_ETHER
      });
    });

    describe('WHEN the challenge is tried to be accepted without covering the bet', function() {
      it('THEN the tx fails', async function() {
        await expectRevert(
          factory.acceptChallenge(challengeCreator, {
            from: anotherUser,
            value: ONE_ETHER.sub(new BN(1))
          }),
          'Not the right bet'
        );
      });
    });
  });

  contract('GIVEN there is a factory of TicTacToes with an open challenge', function([
    anUser,
    anotherUser,
    thirdUser
  ]) {
    const challengeCreator = anUser;
    before(async function() {
      await factory.createChallenge(DEFAULT_MAX_BLOCKS_PER_MOVE, {
        from: challengeCreator,
        value: ONE_ETHER
      });
    });

    describe('WHEN trying to accept a non-existent challenge', function() {
      it('THEN the tx fails', async function() {
        await expectRevert(
          factory.acceptChallenge(thirdUser, {
            from: anotherUser,
            value: ONE_ETHER
          }),
          'Challenge not found'
        );
      });
    });
  });
});
