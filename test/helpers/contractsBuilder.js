const {
  DEFAULT_MAX_BLOCKS_PER_MOVE,
  DEFAULT_MIN_MAX_BLOCKS_PER_MOVE,
  DEFAULT_MAX_MAX_BLOCKS_PER_MOVE,
  ONE_ETHER
} = require('./constants');

const TicTacToeFactory = artifacts.require('TicTacToeFactory');
const TicTacToe = artifacts.require('TicTacToe');

const getFactory = () => this.using.factory || TicTacToeFactory.deployed();

const createFactory = async ({
  minMaxBlocksPerMove = DEFAULT_MIN_MAX_BLOCKS_PER_MOVE,
  maxMaxBlocksPerMove = DEFAULT_MAX_MAX_BLOCKS_PER_MOVE
}) => {
  this.using.factory = await TicTacToeFactory.new(minMaxBlocksPerMove, maxMaxBlocksPerMove);
  return this.using.factory;
};

const createGame = async ({
  playerOne,
  playerTwo,
  maxBlocksPerMove = DEFAULT_MAX_BLOCKS_PER_MOVE,
  bet = ONE_ETHER
}) => {
  const factory = await getFactory();
  await factory.createChallenge(maxBlocksPerMove, { from: playerOne, value: bet });
  const result = await factory.acceptChallenge(playerOne, { from: playerTwo, value: bet });
  return TicTacToe.at(result.logs[0].args.game);
};
module.exports = () => {
  this.using = {};
  return {
    createFactory,
    createGame,
    getFactory
  };
};
