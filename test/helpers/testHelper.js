const constants = require('./constants');
const contractsBuilder = require('./contractsBuilder');
const utilFunctions = require('./utilFunctions');

module.exports = {
  ...constants,
  ...contractsBuilder(),
  ...utilFunctions
};
