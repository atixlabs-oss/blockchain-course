const { expectRevert } = require('openzeppelin-test-helpers');

const { createFactory } = require('./helpers/testHelper');

contract('GIVEN there is an user with enough funds', function() {
  describe('WHEN trying to create a factory with a min max blocks per move bigger than max blocks per move', function() {
    it('THEN the tx fails', async function() {
      await expectRevert(
        createFactory({ minMaxBlocksPerMove: 100, maxMaxBlocksPerMove: 99 }),
        'Min max blocks per move cannot be fewer than max max blocks per move'
      );
    });
  });
});
