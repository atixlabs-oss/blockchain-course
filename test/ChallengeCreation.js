const { expectEvent, expectRevert, BN } = require('openzeppelin-test-helpers');
const { expect, use } = require('chai');

const dirtyChai = require('dirty-chai');
const bnChai = require('bn-chai');

const {
  getFactory,
  DEFAULT_MAX_BLOCKS_PER_MOVE,
  DEFAULT_MAX_MAX_BLOCKS_PER_MOVE,
  DEFAULT_MIN_MAX_BLOCKS_PER_MOVE,
  ONE_ETHER,
  getBalance,
  getGasCost
} = require('./helpers/testHelper');

use(dirtyChai);
use(bnChai(BN));

// TODO Remove skip when ready to make this tests
describe.skip('Factory tests-Challenge creation', function() {
  let factory;
  before(async function() {
    factory = await getFactory();
  });

  contract('GIVEN there is a factory of TicTacToes', function([anUser]) {
    describe('WHEN a challenge is created', function() {
      const challengeCreator = anUser;
      let result;
      let previousBalance;
      let gasCost;
      before(async function() {
        previousBalance = await getBalance(challengeCreator);
        result = await factory.createChallenge(DEFAULT_MAX_BLOCKS_PER_MOVE, {
          from: challengeCreator,
          value: ONE_ETHER
        });
        gasCost = await getGasCost(result);
      });

      it('THEN an event is emitted', async function() {
        expectEvent.inLogs(result.logs, 'ChallengeCreated', {
          challengeCreator,
          bet: ONE_ETHER
        });
      });

      it('THEN the balance of the factory increases', async function() {
        expect(await getBalance(factory.address)).to.eq.BN(ONE_ETHER);
      });
      it('THEN the balance of the challenge creator decreases', async function() {
        expect(await getBalance(challengeCreator)).to.eq.BN(
          previousBalance.sub(ONE_ETHER).sub(gasCost)
        );
      });
    });
  });

  contract('GIVEN there is a factory of TicTacToes', function([anUser]) {
    describe('WHEN a challenge is created with the max max blocks per move', function() {
      const challengeCreator = anUser;
      it('THEN tx succeeds', async function() {
        await factory.createChallenge(DEFAULT_MAX_MAX_BLOCKS_PER_MOVE, {
          from: challengeCreator,
          value: ONE_ETHER
        });
      });
    });
  });
  contract('GIVEN there is a factory of TicTacToes', function([anUser]) {
    describe('WHEN a challenge is created with the min max blocks per move', function() {
      const challengeCreator = anUser;
      it('THEN tx succeeds', async function() {
        await factory.createChallenge(DEFAULT_MIN_MAX_BLOCKS_PER_MOVE, {
          from: challengeCreator,
          value: ONE_ETHER
        });
      });
    });
  });
  contract('GIVEN there is a factory of TicTacToes with an open challenge', function([
    anUser,
    anotherUser
  ]) {
    before(async function() {
      await factory.createChallenge(DEFAULT_MAX_BLOCKS_PER_MOVE, {
        from: anUser,
        value: ONE_ETHER
      });
    });
    describe('WHEN another user creates another challenge', function() {
      it('THEN the tx succeeds', async function() {
        await factory.createChallenge(DEFAULT_MAX_BLOCKS_PER_MOVE, {
          from: anotherUser,
          value: ONE_ETHER
        });
      });
    });
  });

  contract('GIVEN there is a factory of TicTacToes', function([anUser]) {
    const challengeCreator = anUser;

    describe('WHEN a challenge is created with too many maxBlocksPerMove', function() {
      it('THEN the tx fails', async function() {
        await expectRevert(
          factory.createChallenge(DEFAULT_MAX_MAX_BLOCKS_PER_MOVE + 1, {
            from: challengeCreator,
            value: ONE_ETHER
          }),
          'Max blocks per move are too many'
        );
      });
    });
  });

  contract('GIVEN there is a factory of TicTacToes', function([anUser]) {
    const challengeCreator = anUser;

    describe('WHEN a challenge is created with too few of maxBlocksPerMove', function() {
      it('THEN the tx fails', async function() {
        await expectRevert(
          factory.createChallenge(DEFAULT_MIN_MAX_BLOCKS_PER_MOVE - 1, {
            from: challengeCreator,
            value: ONE_ETHER
          }),
          'Max blocks per move are too few'
        );
      });
    });
  });

  contract('GIVEN there is a factory of TicTacToes', function([anUser]) {
    const challengeCreator = anUser;

    describe('WHEN a challenge is created without sending a bet', function() {
      it('THEN the tx fails', async function() {
        await expectRevert(
          factory.createChallenge(DEFAULT_MAX_BLOCKS_PER_MOVE, {
            from: challengeCreator,
            value: 0
          }),
          'You have to make a bet'
        );
      });
    });
  });

  contract('GIVEN there is a factory of TicTacToes with an open challenge', function([anUser]) {
    const challengeCreator = anUser;
    before(async function() {
      // TODO fulfill precondition
    });

    describe('WHEN another challenge is created from the same account', function() {
      it('THEN the tx fails', async function() {
        // TODO assert exception
      });
    });
  });
});
