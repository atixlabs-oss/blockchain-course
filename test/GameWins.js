const { expectEvent, BN } = require('openzeppelin-test-helpers');
const { expect, use } = require('chai');

const dirtyChai = require('dirty-chai');
const bnChai = require('bn-chai');
const { createGame, DEFAULT_BET, getGasCost, getBalance } = require('./helpers/testHelper');

use(dirtyChai);
use(bnChai(BN));

describe('Game tests-Disconnection', function() {
  contract('GIVEN there is an open game', function([playerOne, playerTwo]) {
    let game;
    before(async function() {
      game = await createGame({ playerOne, playerTwo });
      await game.makeMove(0, 0, { from: playerOne });
      await game.makeMove(1, 0, { from: playerTwo });
      await game.makeMove(0, 1, { from: playerOne });
      await game.makeMove(1, 1, { from: playerTwo });
    });
    describe('WHEN the player one wins by a row', function() {
      let result;
      before(async function() {
        result = await game.makeMove(0, 2, { from: playerOne });
      });
      it('THEN the a log is emited', async function() {
        expectEvent.inLogs(result.logs, 'GameWon', { winner: playerOne });
      });
    });
  });

  contract('GIVEN there is an open game', function([playerOne, playerTwo]) {
    let game;
    before(async function() {
      game = await createGame({ playerOne, playerTwo });
      await game.makeMove(0, 0, { from: playerOne });
      await game.makeMove(0, 1, { from: playerTwo });
      await game.makeMove(1, 0, { from: playerOne });
      await game.makeMove(1, 1, { from: playerTwo });
    });
    describe('WHEN the player one wins by a column', function() {
      let result;

      before(async function() {
        result = await game.makeMove(2, 0, { from: playerOne });
      });
      it('THEN the a log is emited', async function() {
        expectEvent.inLogs(result.logs, 'GameWon', { winner: playerOne });
      });
    });
  });

  contract('GIVEN there is an open game', function([playerOne, playerTwo]) {
    let game;
    before(async function() {
      game = await createGame({ playerOne, playerTwo });
      await game.makeMove(0, 0, { from: playerOne });
      await game.makeMove(0, 1, { from: playerTwo });
      await game.makeMove(1, 1, { from: playerOne });
      await game.makeMove(1, 0, { from: playerTwo });
    });
    describe('WHEN the player one wins by a diagonal', function() {
      let result;

      before(async function() {
        result = await game.makeMove(2, 2, { from: playerOne });
      });
      it('THEN the a log is emited', async function() {
        expectEvent.inLogs(result.logs, 'GameWon', { winner: playerOne });
      });
    });
  });

  contract('GIVEN there is an open game', function([playerOne, playerTwo]) {
    let game;
    before(async function() {
      game = await createGame({ playerOne, playerTwo });
      await game.makeMove(2, 0, { from: playerOne });
      await game.makeMove(0, 1, { from: playerTwo });
      await game.makeMove(1, 1, { from: playerOne });
      await game.makeMove(1, 0, { from: playerTwo });
    });
    describe('WHEN the player one wins by the other diagonal', function() {
      let result;

      before(async function() {
        result = await game.makeMove(0, 2, { from: playerOne });
      });
      it('THEN the a log is emited', async function() {
        expectEvent.inLogs(result.logs, 'GameWon', { winner: playerOne });
      });
    });
  });

  contract('GIVEN there is an open game', function([playerOne, playerTwo]) {
    let game;
    before(async function() {
      game = await createGame({ playerOne, playerTwo });
      await game.makeMove(0, 0, { from: playerOne });
      await game.makeMove(1, 0, { from: playerTwo });
      await game.makeMove(0, 1, { from: playerOne });
      await game.makeMove(1, 1, { from: playerTwo });
      await game.makeMove(2, 2, { from: playerOne });
    });
    describe('WHEN the player two wins by another row', function() {
      let result;
      before(async function() {
        result = await game.makeMove(1, 2, { from: playerTwo });
      });
      it('THEN the a log is emited', async function() {
        expectEvent.inLogs(result.logs, 'GameWon', { winner: playerTwo });
      });
    });
  });

  contract('GIVEN there is an open game', function([playerOne, playerTwo]) {
    let game;
    before(async function() {
      game = await createGame({ playerOne, playerTwo });
      await game.makeMove(0, 0, { from: playerOne });
      await game.makeMove(1, 0, { from: playerTwo });
      await game.makeMove(0, 1, { from: playerOne });
      await game.makeMove(1, 1, { from: playerTwo });
      await game.makeMove(2, 2, { from: playerOne });
    });
    describe('WHEN a player wins', function() {
      let result;
      before(async function() {
        result = await game.makeMove(1, 2, { from: playerTwo });
      });
      it('THEN the a log is emited', async function() {
        expectEvent.inLogs(result.logs, 'GameWon', { winner: playerTwo });
      });

      it('THEN an event is emitted', async function() {
        await expectEvent.inLogs(result.logs, 'GameWon', {
          winner: playerTwo
        });
      });

      it('THEN the winner is that player', async function() {
        expect(await game.winner()).to.be.equal(playerTwo);
      });
      it('THEN the playerTwo is assigned all the prize(both bets)', async function() {
        expect(await game.claimablePrize(playerTwo)).to.be.eq.BN(DEFAULT_BET.mul(new BN(2)));
      });

      it('THEN the game has been won', async function() {
        expect(await game.hasBeenWon()).to.be.true();
      });

      it('THEN the game is not a draw', async function() {
        expect(await game.gameIsADraw()).to.be.false();
      });

      it('THEN the game has finished', async function() {
        expect(await game.hasFinished()).to.be.true();
      });
    });
  });

  contract('GIVEN there is a won game', function([playerOne, playerTwo]) {
    let game;
    before(async function() {
      game = await createGame({ playerOne, playerTwo });
      // x x x
      // o o
      //
      await game.makeMove(0, 0, { from: playerOne });
      await game.makeMove(1, 0, { from: playerTwo });
      await game.makeMove(0, 1, { from: playerOne });
      await game.makeMove(1, 1, { from: playerTwo });
      await game.makeMove(0, 2, { from: playerOne });
    });
    describe('WHEN the prize is claimed by the winner', function() {
      let previousPlayerOneBalance;
      let result;
      let gasCost;
      before(async function() {
        previousPlayerOneBalance = await getBalance(playerOne);
        result = await game.claimPrize({ from: playerOne });
        gasCost = await getGasCost(result);
      });
      it('THEN the balance of the game clears', async function() {
        expect(await getBalance(game.address)).to.be.eq.BN(new BN(0));
      });
      it('THEN the balance of the player one increases', async function() {
        expect(await getBalance(playerOne)).to.be.eq.BN(
          previousPlayerOneBalance.add(DEFAULT_BET.mul(new BN(2))).sub(gasCost)
        );
      });
      it('THEN the assigned prize is cleared', async function() {
        expect(await game.claimablePrize(playerOne)).to.be.eq.BN(new BN(0));
      });
      it('THEN an event is emitted', async function() {
        expectEvent.inLogs(result.logs, 'PrizeSent', {
          receiver: playerOne,
          amount: DEFAULT_BET.mul(new BN(2))
        });
      });
    });
  });
});
