const { expectRevert, expectEvent, BN } = require('openzeppelin-test-helpers');
const { expect, use } = require('chai');

const dirtyChai = require('dirty-chai');
const bnChai = require('bn-chai');
const { createGame, DEFAULT_BET, getBalance, getGasCost } = require('./helpers/testHelper');

use(dirtyChai);
use(bnChai(BN));

// TODO Remove skip when ready to run this tests
describe('Game tests-Disconnection', function() {
  contract('GIVEN there is a fresh game', function([playerOne, playerTwo]) {
    let game;
    before(async function() {
      game = await createGame({ playerOne, playerTwo });
      // No marks
    });
    describe('WHEN a player claims the draw', function() {
      it('THEN the tx fails', async function() {
        await expectRevert(
          game.claimDraw({
            from: playerTwo
          }),
          'Cell unmarked'
        );
      });
    });
  });

  contract('GIVEN there is an open game with some marks', function([playerOne, playerTwo]) {
    let game;
    before(async function() {
      game = await createGame({ playerOne, playerTwo });
      // o x o
      // x x o
      // x o

      await game.makeMove(0, 1, { from: playerOne });
      await game.makeMove(0, 0, { from: playerTwo });
      await game.makeMove(1, 0, { from: playerOne });
      await game.makeMove(0, 2, { from: playerTwo });
      await game.makeMove(1, 1, { from: playerOne });
      await game.makeMove(1, 2, { from: playerTwo });
      await game.makeMove(2, 0, { from: playerOne });
      await game.makeMove(2, 1, { from: playerTwo });
    });

    // eslint-disable-next-line mocha/no-identical-title
    describe('WHEN a player claims the draw', function() {
      it('THEN the tx fails', async function() {
        await expectRevert(
          game.claimDraw({
            from: playerTwo
          }),
          'Cell unmarked'
        );
      });
    });
  });

  contract('GIVEN there is a won game', function([playerOne, playerTwo]) {
    let game;
    before(async function() {
      game = await createGame({ playerOne, playerTwo });
      // x x x
      // o o
      //
      await game.makeMove(0, 0, { from: playerOne });
      await game.makeMove(1, 0, { from: playerTwo });
      await game.makeMove(0, 1, { from: playerOne });
      await game.makeMove(1, 1, { from: playerTwo });
      await game.makeMove(0, 2, { from: playerOne });
    });
    // eslint-disable-next-line mocha/no-identical-title
    describe('WHEN a player claims the draw', function() {
      it('THEN the tx fails', async function() {
        await expectRevert(
          game.claimDraw({
            from: playerTwo
          }),
          'Game has already finished'
        );
      });
    });
  });

  contract('GIVEN there is a game that results in a draw', function([playerOne, playerTwo]) {
    let game;
    before(async function() {
      game = await createGame({ playerOne, playerTwo });
      // o x o
      // x x o
      // x o x
      await game.makeMove(0, 1, { from: playerOne });
      await game.makeMove(0, 0, { from: playerTwo });
      await game.makeMove(1, 0, { from: playerOne });
      await game.makeMove(0, 2, { from: playerTwo });
      await game.makeMove(1, 1, { from: playerOne });
      await game.makeMove(1, 2, { from: playerTwo });
      await game.makeMove(2, 0, { from: playerOne });
      await game.makeMove(2, 1, { from: playerTwo });
      await game.makeMove(2, 2, { from: playerOne });
    });
    describe('WHEN the draw is claimed', function() {
      let result;
      before(async function() {
        result = await game.claimDraw();
      });
      it('THEN an event is emmited', async function() {
        expectEvent.inLogs(result.logs, 'Draw', {});
      });

      it('THEN half of the prize is assigned to player one', async function() {
        expect(await game.claimablePrize(playerOne)).to.be.eq.BN(DEFAULT_BET);
      });
      it('THEN half of the prize is assigned to player two', async function() {
        expect(await game.claimablePrize(playerTwo)).to.be.eq.BN(DEFAULT_BET);
      });
      it('THEN the draw cannot be claimed again', async function() {
        await expectRevert(
          game.claimDraw({
            from: playerTwo
          }),
          'Game has already finished'
        );
      });
      it('THEN the game is finished', async function() {
        // TODO check if the game has finished
      });
      it('THEN the game says it is a draw', async function() {
        expect(await game.gameIsADraw()).to.be.true();
      });
      it('THEN the game is not won', async function() {
        expect(await game.hasBeenWon()).to.be.false();
      });
    });
  });

  contract('GIVEN a draw', function([playerOne, playerTwo]) {
    let game;
    before(async function() {
      game = await createGame({ playerOne, playerTwo });
      // o x o
      // x x o
      // x o x
      await game.makeMove(0, 1, { from: playerOne });
      await game.makeMove(0, 0, { from: playerTwo });
      await game.makeMove(1, 0, { from: playerOne });
      await game.makeMove(0, 2, { from: playerTwo });
      await game.makeMove(1, 1, { from: playerOne });
      await game.makeMove(1, 2, { from: playerTwo });
      await game.makeMove(2, 0, { from: playerOne });
      await game.makeMove(2, 1, { from: playerTwo });
      await game.makeMove(2, 2, { from: playerOne });
      await game.claimDraw();
    });
    describe('WHEN the prize is claimed by player one', function() {
      let previousGameBalance;
      let previousPlayerOneBalance;
      let result;
      let gasCost;
      before(async function() {
        previousGameBalance = await getBalance(game.address);
        previousPlayerOneBalance = await getBalance(playerOne);
        result = await game.claimPrize({ from: playerOne });
        gasCost = await getGasCost(result);
      });
      it('THEN the balance of the game decreases', async function() {
        expect(await getBalance(game.address)).to.be.eq.BN(previousGameBalance.sub(DEFAULT_BET));
      });
      it('THEN the balance of the player one increases', async function() {
        expect(await getBalance(playerOne)).to.be.eq.BN(
          previousPlayerOneBalance.add(DEFAULT_BET).sub(gasCost)
        );
      });
      it('THEN the assigned prize is cleared', async function() {
        expect(await game.claimablePrize(playerOne)).to.be.eq.BN(new BN(0));
      });
      it('THEN an event is emitted', async function() {
        expectEvent.inLogs(result.logs, 'PrizeSent', {
          receiver: playerOne,
          amount: DEFAULT_BET
        });
      });
    });
  });

  contract('GIVEN a draw', function([playerOne, playerTwo]) {
    let game;
    before(async function() {
      game = await createGame({ playerOne, playerTwo });
      // o x o
      // x x o
      // x o x
      await game.makeMove(0, 1, { from: playerOne });
      await game.makeMove(0, 0, { from: playerTwo });
      await game.makeMove(1, 0, { from: playerOne });
      await game.makeMove(0, 2, { from: playerTwo });
      await game.makeMove(1, 1, { from: playerOne });
      await game.makeMove(1, 2, { from: playerTwo });
      await game.makeMove(2, 0, { from: playerOne });
      await game.makeMove(2, 1, { from: playerTwo });
      await game.makeMove(2, 2, { from: playerOne });
      await game.claimDraw();
    });
    describe('WHEN the both players claim the prize', function() {
      let previousPlayerOneBalance;
      let previousPlayerTwoBalance;
      let resultPlayerOne;
      let resultPlayerTwo;
      let gasCostPlayerOne;
      let gasCostPlayerTwo;
      before(async function() {
        previousPlayerOneBalance = await getBalance(playerOne);
        previousPlayerTwoBalance = await getBalance(playerTwo);
        resultPlayerOne = await game.claimPrize({ from: playerOne });
        resultPlayerTwo = await game.claimPrize({ from: playerTwo });
        gasCostPlayerOne = await getGasCost(resultPlayerOne);
        gasCostPlayerTwo = await getGasCost(resultPlayerTwo);
      });
      it('THEN the balance of the game is cleared', async function() {
        // TODO Check that the balance of the game has decreased
      });
      it('THEN the balance of player one increases', async function() {
        // TODO check that the player one has been sent the bet
        // Hint be careful to take into account the gasCost
      });
      it('THEN the balance of player two increases', async function() {
        // TODO check that the player one has been sent the bet
        // Hint be careful to take into account the gasCost
      });
      it('THEN the assigned prize to the player one is cleared', async function() {
        expect(await game.claimablePrize(playerOne)).to.be.eq.BN(new BN(0));
      });
      it('THEN the assigned prize to the player two is cleared', async function() {
        expect(await game.claimablePrize(playerTwo)).to.be.eq.BN(new BN(0));
      });
      it('THEN an event is emitted for the player one', async function() {
        expectEvent.inLogs(resultPlayerOne.logs, 'PrizeSent', {
          receiver: playerOne,
          amount: DEFAULT_BET
        });
      });

      it('THEN an event is emitted for the player two', async function() {
        expectEvent.inLogs(resultPlayerTwo.logs, 'PrizeSent', {
          receiver: playerTwo,
          amount: DEFAULT_BET
        });
      });
    });
  });
});
